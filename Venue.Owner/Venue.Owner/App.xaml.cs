﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Venue.Owner.Services;
using Venue.Owner.Views;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Venue.Owner
{
    public partial class App : Application
    {
        //TODO: Replace with *.azurewebsites.net url after deploying backend to Azure
        public static string AzureBackendUrl = "http://localhost:5000";
        public static bool UseMockDataStore = true;

        public App()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTM4MzQzQDMxMzcyZTMyMmUzMGYvR25FOFZDWm0zN1RSOGo3VDJId3FMenh2R2RXSWlpYXd0emRXUk01Vm89");
            InitializeComponent();

            if (UseMockDataStore)
                DependencyService.Register<MockDataStore>();
            else
                DependencyService.Register<AzureDataStore>();

            MainPage = new HomePage();
            
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
