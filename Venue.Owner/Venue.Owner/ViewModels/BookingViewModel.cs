﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Venue.Owner.DataService;
using Venue.Owner.Models;
using Xamarin.Forms;

namespace Venue.Owner.ViewModels
{
    public class BookingViewModel 
        : INotifyPropertyChanged
    {

        public Task Initialization { get; private set; }
        private string _customerMobile;
        private string _customerName;
        private Guid _userId;
        UserDataService service;
        private DateTime _selectedDate;
        public BookingViewModel()
        {
            service = new UserDataService();
            BookSelectCommand = new Command(BookingClickEvent);
            CheckCustomerCommand = new Command(CheckCustomerClickEvent);
            BookUnSelectCommand = new Command(BookingUnClickEvent);
            BookCommand = new Command(BookClickEvent);
            FindCommand = new Command(FindClickEvent);

            #region AllVenues

            Initialization = InitializeAsync();

            #endregion

            #region AllFacilities

            //VenueFacilities = new List<VenueFacilitiesModel>();
            //VenueFacilities.Add(new VenueFacilitiesModel { Id = Guid.NewGuid(), Name= "Water" });
            //VenueFacilities.Add(new VenueFacilitiesModel { Id = Guid.NewGuid(), Name = "Snacks" });
            //VenueFacilities.Add(new VenueFacilitiesModel { Id = Guid.NewGuid(), Name = "Cool Drinks" });
            //VenueFacilities.Add(new VenueFacilitiesModel { Id = Guid.NewGuid(), Name = "Cola" });
            //VenueFacilities.Add(new VenueFacilitiesModel { Id = Guid.NewGuid(), Name = "Pepsi" });
            //VenueFacilities.Add(new VenueFacilitiesModel { Id = Guid.NewGuid(), Name = "Dew" });
            //VenueFacilities.Add(new VenueFacilitiesModel { Id = Guid.NewGuid(), Name = "Sun Top" });

            //OnPropertyChanged("VenueFacilities");

            #endregion


            IsTimePanelFocused = true;
            OnPropertyChanged("Allbookings");

            #region Datepicker values

            ObservableCollection<object> todaycollection = new ObservableCollection<object>();

            //Select today dates
            todaycollection.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Date.Month).Substring(0, 3));
            if (DateTime.Now.Date.Day < 10)
                todaycollection.Add("0" + DateTime.Now.Date.Day);
            else
                todaycollection.Add(DateTime.Now.Date.Day.ToString());
            todaycollection.Add(DateTime.Now.Date.Year.ToString());

            this.StartDate = todaycollection;

            #endregion
        }

        private void CheckCustomerClickEvent(object obj)
        {
            try
            {
                IsCheckBusy = true;

                if (string.IsNullOrEmpty(CustomerMobile))
                    throw new Exception("The mobile number or email is null or empty");

                var response = service.GetUserByEmailOrMobile(CustomerMobile).Result;

                if (response != null)
                {
                    CustomerName = response.Name;
                    UserId = response.Id;
                }

                IsCheckBusy = false;
            }
            catch (Exception ex)
            {
                IsCheckBusy = false;
                PopUpMessage = ex.Message;
                IsPopUpOpen = true;

            }
        }

        public async Task InitializeAsync()
        {
            // Asynchronously wait for the fundamental instance to initialize.

            try
            {
                VenueDataService service = new VenueDataService();

                StartDate = new ObservableCollection<object>();
                IsBusy = true;

                var response = await service.GetAllVenues(Guid.Parse("63DF381A-F4E4-E911-8417-00155D01642E"),false);

                AllVenues = null;
                AllVenues = response;
                IsBusy = false;


            }
            catch (Exception ex)
            {
                IsBusy = false;
                //throw;
                PopUpMessage = ex.Message;
                IsPopUpOpen = true;

            }
        }

        private void BookingUnClickEvent()
        {
            BookingListChanged();
        }

        private async void BookClickEvent(object obj)
        {
            try
            {
                if(IsTimePanelFocused)
                {
                    IsTimePanelFocused = false;
                }
                else
                {
                    
                    if (string.IsNullOrEmpty(CustomerMobile))
                        throw new Exception("The mobile number is null or empty");
                    if (string.IsNullOrEmpty(CustomerName))
                        throw new Exception("The customer name is null or empty");

                    IsBusy = true;
                    ReservationDataService service = new ReservationDataService();
                    List<AddReservationModel> addReservationModels = new List<AddReservationModel>();


                    foreach (var item in BookedItems)
                    {
                        addReservationModels.Add(new Models.AddReservationModel
                        {
                            Id = Guid.NewGuid(),
                            Price = SelectedVenueItem.Price,
                            ReservationDate =  SelectedDate,
                            ReservationFromTime =item.Fromtime,
                            ReservationToTime = item.ToTime,
                            UserId = Guid.Parse("63DF381A-F4E4-E911-8417-00155D01642E"),
                            VenueItemId = SelectedVenueItem.Id,

                        });
                    }
                    var response = await service.AddReservationService(addReservationModels);

                    if(response.Item1)
                    {
                        PopUpMessage = "Reservations added successfully !";
                        IsPopUpOpen = true;
                        IsTimePanelFocused = true;
                        FindClickEvent(null);
                    }
                    IsBusy = false;


                }
                


            }
            catch (Exception ex)
            {

                IsBusy = false;

                PopUpMessage = ex.Message;
                IsPopUpOpen = true;

            }

            //throw new NotImplementedException();
        }

        private async void FindClickEvent(object obj)
        {
            //throw new NotImplementedException();
            try
            {
                if (SelectedVenue == null)
                    throw new Exception("Select the site");
                if (SelectedVenueItem == null)
                    throw new Exception("Select the venue item");

                ReservationDataService service = new ReservationDataService();

                IsBusy = true;

                var response = await service.GetAllReservation("a4anu10@gmail.com");

                AllReservations = null;
                AllReservations = response;

                #region Fill all reservation


                Allbookings = new List<BookingModel>();
                var bookings = new List<BookingModel>();
                int duration = 60;
                int countReservation = 1;
                for (var ts = TimeSpan.FromMinutes(SelectedVenue.OpenTime); ts <= TimeSpan.FromMinutes(SelectedVenue.CloseTime); ts = ts.Add(new TimeSpan(0, duration, 0)))
                {
                    var fromtime = DateTime.Today.Add(ts);
                    var totime = DateTime.Today.Add(ts).AddMinutes(duration);

                    #region Check if it is reserverd

                    string ReservedBy = string.Empty;
                    string ReservedPhone = string.Empty;
                    bool isReserved = false;
                   
                    var reservation = AllReservations.SingleOrDefault(a => a.ReservationFrom <= Convert.ToInt32(ts.TotalMinutes) && a.ReservationTo >= Convert.ToInt32(ts.TotalMinutes + duration));

                    if (reservation != null)
                    {
                        isReserved = true;
                        ReservedBy = reservation.ReservedByName;
                        ReservedPhone = reservation.ReservedByNumber;
                    }


                    #endregion

                    var date = StartDate;
                    bookings.Add(new BookingModel
                    {
                        Id = countReservation,
                        BookingTime = fromtime.ToString("hh:mm tt") + " - " + totime.ToString("hh:mm tt"),
                        //BookNow = !isReserved,
                        IsPaid = false,
                        IsReserved = isReserved,
                        ReservedByNumber = ReservedPhone,
                        ReservedByName = ReservedBy,
                        Fromtime = Convert.ToInt32(fromtime.TimeOfDay.TotalMinutes),
                        ToTime = Convert.ToInt32(totime.TimeOfDay.TotalMinutes),
                        

                    }); 

                    countReservation = countReservation + 1;
                }

                #endregion


                IsBusy = false;

                #region All Bookings Dummy Data

              
                //bookings.Add(new BookingModel
                //{
                //    Id = 1,
                //    BookingTime = "10:00 AM - 10:30 AM",
                //    BookNow = false,
                //    IsPaid = false,
                //    IsReserved = false,
                //    ReservedByNumber = "99038552",
                //    ReservedByName = "Anurag"
                //});
                //bookings.Add(new BookingModel
                //{
                //    Id = 2,
                //    BookingTime = "10:30 AM - 11:00 AM",
                //    BookNow = false,
                //    IsPaid = false,
                //    IsReserved = false,
                //    ReservedByNumber = "99038552",
                //    ReservedByName = "Anurag"
                //});
                //bookings.Add(new BookingModel
                //{
                //    Id = 3,
                //    BookingTime = "11:00 AM - 11:30 AM",
                //    BookNow = false,
                //    IsPaid = false,
                //    IsReserved = true,
                //    ReservedByNumber = "99038552",
                //    ReservedByName = "Anurag"
                //});
                //bookings.Add(new BookingModel
                //{
                //    Id = 4,
                //    BookingTime = "11:30 AM - 12:00 PM",
                //    BookNow = false,
                //    IsPaid = false,
                //    IsReserved = false,
                //    ReservedByNumber = "99038552",
                //    ReservedByName = "Anurag"
                //});
                //bookings.Add(new BookingModel
                //{
                //    Id = 5,
                //    BookingTime = "11:30 AM - 12:00 PM",
                //    BookNow = false,
                //    IsPaid = false,
                //    IsReserved = false,
                //    ReservedByNumber = "99038552",
                //    ReservedByName = "Anurag"
                //});
                //bookings.Add(new BookingModel
                //{
                //    Id = 6,
                //    BookingTime = "12:00 PM - 12:30 PM",
                //    BookNow = false,
                //    IsPaid = false,
                //    IsReserved = false,
                //    ReservedByNumber = "99038552",
                //    ReservedByName = "Anurag"
                //});
                //bookings.Add(new BookingModel
                //{
                //    Id = 7,
                //    BookingTime = "12:00 PM - 12:30 PM",
                //    BookNow = false,
                //    IsPaid = false,
                //    IsReserved = false,
                //    ReservedByNumber = "99038552",
                //    ReservedByName = "Anurag"
                //});
                Allbookings = bookings;
                BookingListChanged();


                #endregion
            }
            catch (Exception ex)
            {
                PopUpMessage = ex.Message;
                IsPopUpOpen = true;
               
            }
           
        }

        private void BookingClickEvent(object obj)
        {
            try
            {
                if (obj != null)
                {
                    var book = obj as BookingModel;
                    var item = Allbookings.FirstOrDefault(a => a.Id == book.Id);
                    if (item != null)
                    {
                        item.BookNow = true;
                        BookingListChanged();
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private void BookingListChanged()
        {
            OnPropertyChanged("Allbookings");
            OnPropertyChanged("BookedItems");

            if (allbookings != null)
            {
                var anybookitem = allbookings.Any(a => a.BookNow == true);
                if (anybookitem)
                {
                    IsBookNowPanelVisible = true;
                }
                else
                {
                    IsBookNowPanelVisible = false;
                }
            }
            else
            {
                IsBookNowPanelVisible = false;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private List<BookingModel> allbookings;
        private List<VenueFacilitiesModel> venueFacilities;
        private BookModel _selectedBookModel;
        private bool _isBookNowPanelVisible;
        private bool _isTimePanelFocused;
        private bool _isPopUpOpen;
        private string _popUpMessage;
        private string _selectedVenueText;
        private List<VenueModel> _allVenues;
        private VenueModel _selectedVenue;
        private List<VenueItemModel> _allVenueItems;
        private VenueItemModel _selectedVenueItem;
        public ICommand BookCommand { get; }
        public ICommand FindCommand { get;  }
        public ICommand BookSelectCommand { get; }
        public ICommand CheckCustomerCommand { get; }

    public ICommand BookUnSelectCommand { get; }

        public virtual List<BookingModel> Allbookings {
            get { return allbookings; }
            set
            {
                allbookings = value;
                BookingListChanged();
            }
        }

        public virtual List<BookingModel> BookedItems
        {
            get {
                if (allbookings != null)
                {
                    return allbookings.Where(a => a.BookNow == true).ToList();
                }
                return allbookings;

            }
            

        }
        void OnListChange(object sender, ListChangedEventArgs e) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Allbookings)));
        public BookModel SelectedBookModel
        {
            get { return _selectedBookModel; }
            set
            {
                _selectedBookModel = value;
                OnPropertyChanged("SelectedBookModel");

            }
        }

        public bool IsBookNowPanelVisible
        {
            get { return _isBookNowPanelVisible; }
            set
            {
                _isBookNowPanelVisible = value;
                OnPropertyChanged("IsBookNowPanelVisible");

            }
        }

        public string SelectedVenueText
        {
            get { return _selectedVenueText; }            
        }

        public bool IsTimePanelFocused
        {
            get { return _isTimePanelFocused; }
            set
            {
                _isTimePanelFocused = value;
                OnPropertyChanged("IsTimePanelFocused");
            }
        }

        public List<VenueFacilitiesModel> VenueFacilities
        {
            get { return venueFacilities; }
            set
            {
                venueFacilities = value;
                OnPropertyChanged("VenueFacilities");
            }
        }

        public bool IsPopUpOpen {
            get { return _isPopUpOpen; }
            set
            {
                _isPopUpOpen = value;
                OnPropertyChanged("IsPopUpOpen");
            }
        }

        public string PopUpMessage
        {
            get { return _popUpMessage; }
            set
            {
                _popUpMessage = value;
                OnPropertyChanged("PopUpMessage");
            }
        }

        public List<ReservationsModel> AllReservations
        {
            get { return _allReservations; }
            set
            {
                _allReservations = value;
                OnPropertyChanged("AllReservations");
            }
        }

        public List<VenueModel> AllVenues {
            get { return _allVenues; }
            set
            {
                _allVenues = value;
                OnPropertyChanged("AllVenues");
            }
        }
        public VenueItemModel SelectedVenueItem
        {
            get { return _selectedVenueItem; }
            set
            {
                _selectedVenueItem = value;
                OnPropertyChanged("SelectedVenueItem");
            }
        }
        public VenueModel SelectedVenue {
            get { return _selectedVenue; }
            set
            {
                _selectedVenue = value;
                OnPropertyChanged("SelectedVenue");
            }
        }
        public List<VenueItemModel> AllVenueItems {
            get { return _allVenueItems; }
            set
            {
                _allVenueItems = value;
                OnPropertyChanged("AllVenueItems");
            }
        }
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }

        public string CustomerMobile
        {
            get { return _customerMobile; }
            set
            {
                _customerMobile = value;
                OnPropertyChanged("CustomerMobile");
            }
        }
        public string CustomerName
        {
            get { return _customerName; }
            set
            {
                _customerName = value;
                OnPropertyChanged("CustomerName");
            }
        }



        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        #region Custom Date Picker 

        private ObservableCollection<object> _startdate;
        private bool _isBusy;
        private List<ReservationsModel> _allReservations;
        private bool _isCheckBusy;

        public ObservableCollection<object> StartDate
        {
            get { return _startdate; }
            set { _startdate = value; OnPropertyChanged("StartDate"); }
        }

        public Guid UserId { get => _userId; set => _userId = value; }
        public bool IsCheckBusy
        {
            get { return _isCheckBusy; }
            set
            {
                _isCheckBusy = value;
                OnPropertyChanged("IsCheckBusy");
            }
        }

        public DateTime SelectedDate { get => _selectedDate; set => _selectedDate = value; }







        #endregion
    }
}
