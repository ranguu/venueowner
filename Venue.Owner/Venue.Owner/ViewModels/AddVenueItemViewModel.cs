﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Venue.Owner.DataService;
using Venue.Owner.Models;
using Xamarin.Forms;

namespace Venue.Owner.ViewModels
{
    public class AddVenueItemViewModel : INotifyPropertyChanged
    {
        private VenueItemModel _venueItem;

        public AllVenuesViewModel AllVenueViewModel { get; }
        public INavigation Navigation { get; private set; }

        private VenueModel _selectedVenue;
        private bool _isPopUpOpen;
        private string _popUpMessage;
        private List<string> _venueSizeList;
        private bool _isBusy;

        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand SaveCommand { get; }

        public AddVenueItemViewModel(VenueModel venueModel,AllVenuesViewModel allVenuesViewModel , INavigation navigation)
        {
            AllVenueViewModel = allVenuesViewModel;
            Navigation = navigation;
            _selectedVenue = venueModel;
            _venueItem = new VenueItemModel();
            _venueSizeList = new List<string>();
            _venueSizeList.Add("_5x5");
            _venueSizeList.Add("_6x6");
            _venueSizeList.Add("_7x7");
            _venueSizeList.Add("_11x11");
            _venueSizeList.Add("SaperateField");

            SaveCommand = new Command(SaveClickEvent);
        }

        private async void SaveClickEvent(object obj)
        {
            try
            {
                if (_selectedVenue == null)
                    throw new Exception("No venue reference found");
                if (string.IsNullOrWhiteSpace(_venueItem.Description))
                    throw new Exception("Description is empty");
                if (string.IsNullOrWhiteSpace(_venueItem.VenueSize))
                    throw new Exception("venue size is empty");
                if (_venueItem.Price == 0)
                    throw new Exception("court fees cannot be zero");



                VenueDataService service = new VenueDataService();

                IsBusy = true;

                _venueItem.Id = Guid.NewGuid();
                _venueItem.VenueId = SelectedVenue.VenueId;

                var response = await service.AddVenueItemService(_venueItem);

                IsBusy = false;
                List<VenueModel> listmodel = AllVenueViewModel.AllVenues.ToList();
                var sltVenue = listmodel.SingleOrDefault(a => a.VenueId == SelectedVenue.VenueId);
                if (sltVenue != null)
                {
                    if(sltVenue.VenueItems == null)
                        sltVenue.VenueItems = new List<VenueItemModel>();

                    if (SelectedVenue.VenueItems == null)
                    {

                        var ven = (VenueModel)SelectedVenue;
                        ven.VenueItems = null;
                        ven.VenueItems.Add(_venueItem);

                        SelectedVenue = null;
                        SelectedVenue = ven;

                      
                        AllVenueViewModel.AllVenues = null;
                        AllVenueViewModel.AllVenues = listmodel;

                    }
                    else
                    {
                        var temvenuitems = SelectedVenue.VenueItems.ToList();
                        temvenuitems.Add(_venueItem);
                        var ven = (VenueModel)SelectedVenue;
                        ven.VenueItems = null;
                        ven.VenueItems = temvenuitems;

                        SelectedVenue = null;
                        SelectedVenue = ven;
                        AllVenueViewModel.AllVenues = null;
                        AllVenueViewModel.AllVenues = listmodel;
                    }
                }


                await Navigation.PopModalAsync();

            }
            catch (Exception ex)
            {
                PopUpMessage = ex.Message;
                IsPopUpOpen = true;
            }
        }

        public VenueItemModel VenueItem
        {
            get { return _venueItem; }
            set
            {
                _venueItem = value;
                OnPropertyChanged("VenueItem");
            }
        }

        public VenueModel SelectedVenue
        {
            get { return _selectedVenue; }
            set
            {
                _selectedVenue = value;
                OnPropertyChanged("SelectedVenue");
            }
        }

        public bool IsPopUpOpen
        {
            get { return _isPopUpOpen; }
            set
            {
                _isPopUpOpen = value;
                OnPropertyChanged("IsPopUpOpen");
            }
        }

        public string PopUpMessage
        {
            get { return _popUpMessage; }
            set
            {
                _popUpMessage = value;
                OnPropertyChanged("PopUpMessage");
            }
        }

        public List<string> VenueSizeList
        {
            get { return _venueSizeList; }
            set
            {
                _venueSizeList = value;
                OnPropertyChanged("VenueSizeList");
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
