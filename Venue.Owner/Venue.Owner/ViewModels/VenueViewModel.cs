﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Venue.Owner.Models;

namespace Venue.Owner.ViewModels
{
   public  class VenueViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
      
        private VenueModel _venue;

        public VenueViewModel(VenueModel venueModel)
        {
            _venue = venueModel;
        }


        public VenueModel Venue
        {
            get { return _venue; }
            set
            {
                _venue = value;
                OnPropertyChanged("Venue");
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



    }
}
