﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace Venue.Owner.ViewModels
{
    public class ChooseLocationViewModel
    {
       
       // IGoogleMapsApiService googleMapsApi = new GoogleMapsApiService();
        public bool HasRouteRunning { get; set; }
        public ICommand GetLocationNameCommand { get; set; }
        public bool IsRouteNotRunning { get { return !HasRouteRunning; } }

        public ChooseLocationViewModel()
        {
            GetLocationNameCommand = new Command<Position>(async (param) => await GetLocationName(param));
        }


        //Get place 
        public async Task GetLocationName(Position position)
        {
            try
            {
                var placemarks = await Geocoding.GetPlacemarksAsync(position.Latitude, position.Longitude);
                var placemark = placemarks?.FirstOrDefault();
                if (placemark != null)
                {
                    //PickupText = placemark.FeatureName;
                }
                else
                {
                    //PickupText = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }
    }
}
