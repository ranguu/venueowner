﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Venue.Owner.DataService;
using Venue.Owner.Models;
using Xamarin.Forms;

namespace Venue.Owner.ViewModels
{
    public class AllVenuesViewModel: INotifyPropertyChanged, IAsyncInitialization
    {

        public event PropertyChangedEventHandler PropertyChanged;
        private List<VenueModel> _allVenues;
        private VenueModel _selectedVenue;
        private List<VenueItemModel> _allVenueItems;
        private bool _isBusy;
        private bool _showReloadButton;


        public Task Initialization { get; private set; }
        public ICommand ReloadCommand { get; }

        public AllVenuesViewModel()
        {
            ReloadCommand = new Command(ReloadClickEvent);

            #region Dummy Data


            //_allVenues = new List<VenueModel>();
            //List<VenueItemModel> venueItems = new List<VenueItemModel>();
            //venueItems.Add(new VenueItemModel { Id = Guid.NewGuid(), VenueItemName = "8 X 8", Price = 5, Description = "Good playground recommended" });
            //venueItems.Add(new VenueItemModel { Id = Guid.NewGuid(), VenueItemName = "9 X 9", Price = 10 });
            //venueItems.Add(new VenueItemModel { Id = Guid.NewGuid(), VenueItemName = "10 X 10", Price = 15, Description = "Biggest ground" });

            //List<string> facilites = new List<string>();
            //facilites.Add("footballs");
            //facilites.Add("water");
            //facilites.Add("prayer room");

            //_allVenues.Add(new VenueModel
            //{
            //    Area = "Area",
            //    Country = "Oman",
            //    CloseTime = 1300,
            //    Latitude = 4567.23,
            //    Longitude = 1223.345,
            //    OpenTime = 460,
            //    State = "Muscat",
            //    VenueId = Guid.NewGuid(),
            //    VenueName = "Azhaiba",
            //    VenueItems = venueItems,
            //    Facilities = facilites


            //});


            //_allVenues.Add(new VenueModel
            //{
            //    Area = "Ghala",
            //    Country = "Oman",
            //    CloseTime = 1300,
            //    Latitude = 4567.23,
            //    Longitude = 1223.345,
            //    OpenTime = 460,
            //    State = "Muscat",
            //    VenueId = Guid.NewGuid(),
            //    VenueName = "Seeb",
            //    VenueItems = venueItems,
            //    Facilities = facilites

            //});

            #endregion

            Initialization = InitializeAsync();
        }

        private void ReloadClickEvent(object obj)
        {
            Initialization = InitializeAsync();
        }

        public async Task InitializeAsync()
        {
            // Asynchronously wait for the fundamental instance to initialize.

            try
            {
                VenueDataService service = new VenueDataService();

                IsBusy = true;
                ShowReloadButton = false;
                var response = await service.GetAllVenues(Guid.Parse("63DF381A-F4E4-E911-8417-00155D01642E"),true);

               
                AllVenues = null;
                AllVenues = response;
                IsBusy = false;


            }
            catch (Exception)
            {
                IsBusy = false;
                OnPropertyChanged("ShowReloadButton");
                //throw;
            }
        }
        public List<VenueModel> AllVenues
        {
            get { return _allVenues; }
            set
            {
                _allVenues = value;
                OnPropertyChanged("AllVenues");
                OnPropertyChanged("ShowReloadButton");
            }
        }

        public VenueModel SelectedVenue
        {
            get { return _selectedVenue; }
            set
            {
                _selectedVenue = value;
                OnPropertyChanged("SelectedVenue");
            }
        }

        public List<VenueItemModel> AllVenueItems
        {
            get { return _allVenueItems; }
            set
            {
                _allVenueItems = value;
                OnPropertyChanged("AllVenueItems");
            }
        }


        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged("IsBusy");
                OnPropertyChanged("ShowReloadButton");
            }
        }

        public bool ShowReloadButton {
            get {
                if (IsBusy)
                    return false;
                if (AllVenues == null)
                    return true;
                if (AllVenues.Count == 0)
                    return true;

                return _showReloadButton;
            }
            set
            {
                _showReloadButton = value;
                OnPropertyChanged("ShowReloadButton");
                OnPropertyChanged("IsBusy");
                OnPropertyChanged("AllVenues");
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
