﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Venue.Owner.DataService;
using Venue.Owner.Models;
using Xamarin.Forms;

namespace Venue.Owner.ViewModels
{
  public   class AddVenueViewModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        private List<string> _countries;
        private List<string> _states;
        private Dictionary<TimeSpan, string> _opentimes;
        private Dictionary<TimeSpan, string> _closetimes;
        private string _selectedCountry;
        private string _selectedState;
        private Dictionary<TimeSpan, string> _selectedOpenTime;
        private Dictionary<TimeSpan, string> _selectedCloseTime;
        private bool _isPopUpOpen;
        private string _popUpMessage;
        private string _selectedarea;
        private string _selectedSiteName;
        private bool _isBusy;
        public INavigation Navigation { get; set; }
        public AllVenuesViewModel AllVenueViewModel { get; set; }
        public ICommand SaveCommand { get; }

        public AddVenueViewModel(INavigation navigation,AllVenuesViewModel allVenuesViewModel)
        {
            Navigation = navigation;
            AllVenueViewModel = allVenuesViewModel;
            
            #region countries list

            _countries = new List<string>();
            _countries.Add("Oman");
            _countries.Add("UAE");
            _countries.Add("Qatar");
            _countries.Add("Saudi");

            #endregion
            #region State

            _states = new List<string>();
            _states.Add("Muscat");
            _states.Add("Ruwi");
            _states.Add("Ghala");
            _states.Add("Burami");

            #endregion
            #region OpenTime & Endtime

            _opentimes = new Dictionary<TimeSpan, string>();
            _closetimes = new Dictionary<TimeSpan, string>();
            for (var ts = new TimeSpan(0, 0, 0); ts <= new TimeSpan(24, 0, 0); ts = ts.Add(new TimeSpan(0, 60, 0)))
            {
                _opentimes.Add(ts, DateTime.Today.Add(ts).ToString("hh:mm tt"));
                _closetimes.Add(ts, DateTime.Today.Add(ts).ToString("hh:mm tt"));
            }
            #endregion


            SaveCommand = new Command(SaveClickEvent);

            
        }

        private async void SaveClickEvent(object obj)
        {
            try
            {
                if (SelectedCountry == null)
                    throw new Exception("select the contry");

                if (SelectedState == null)
                    throw new Exception("select the state");

                if (string.IsNullOrWhiteSpace(Selectedarea))
                    throw new Exception("enter the area");

                if (string.IsNullOrWhiteSpace(SelectedSiteName))
                    throw new Exception("enter the site name");


                if (SelectedOpentime == null)
                    throw new Exception("select the open time");

                if (SelectedClosetime == null)
                    throw new Exception("select the close time");

                VenueDataService service = new VenueDataService();

                IsBusy = true;

                VenueModel newVenue = new VenueModel
                {
                    CloseTime = Convert.ToInt32(SelectedClosetime.FirstOrDefault().Key.TotalMinutes),
                    Country = SelectedCountry,
                    Latitude = 12312,
                    Longitude = 123123,
                    OpenTime = Convert.ToInt32(SelectedOpentime.FirstOrDefault().Key.TotalMinutes),
                    VenueName = SelectedSiteName,
                    UserId = Guid.Parse("63DF381A-F4E4-E911-8417-00155D01642E"),
                    Area = Selectedarea,
                    State = SelectedState,
                    Description = "Active",
                    VenueId = Guid.NewGuid()

                };

                var response = await service.AddVenueService(newVenue);

                IsBusy = false;
                List<VenueModel> listmodel = AllVenueViewModel.AllVenues.ToList();
                listmodel.Add(newVenue);
                AllVenueViewModel.AllVenues = null;
                AllVenueViewModel.AllVenues = listmodel;


                await Navigation.PopModalAsync();

            }
            catch (Exception ex)
            {
                IsBusy = false;
                PopUpMessage = ex.Message;
                IsPopUpOpen = true;
            }
        }

        private bool Cansave(object arg)
        {
            throw new NotImplementedException();
        }

        public List<string> Countries
        {
            get { return _countries; }
            set
            {
                _countries = value;
                OnPropertyChanged("Countries");
            }
        }
        public List<string> States
        {
            get { return _states; }
            set
            {
                _states = value;
                OnPropertyChanged("States");
            }
        }

        public Dictionary<TimeSpan, string> Closetimes
        {
            get { return _closetimes; }
            set
            {
                _closetimes = value;
                OnPropertyChanged("Closetimes");
            }
        }
        public Dictionary<TimeSpan, string> Opentimes
        {
            get { return _opentimes; }
            set
            {
                _opentimes = value;
                OnPropertyChanged("Opentimes");
            }
        }

        public string SelectedCountry
        {
            get { return _selectedCountry; }
            set
            {
                _selectedCountry = value;
                OnPropertyChanged("SelectedCountry");
            }
        }

        public string SelectedState
        {
            get { return _selectedState; }
            set
            {
                _selectedState = value;
                OnPropertyChanged("SelectedState");
            }
        }
        public Dictionary<TimeSpan,string> SelectedOpentime {
            get { return _selectedOpenTime; }
            set
            {
                _selectedOpenTime = value;
                OnPropertyChanged("SelectedOpentime");
            }
        }
        public Dictionary<TimeSpan, string> SelectedClosetime {
            get { return _selectedCloseTime; }
            set
            {
                _selectedCloseTime = value;
                OnPropertyChanged("SelectedClosetime");
            }
        }

        public bool IsPopUpOpen
        {
            get { return _isPopUpOpen; }
            set
            {
                _isPopUpOpen = value;
                OnPropertyChanged("IsPopUpOpen");
            }
        }

        public string PopUpMessage
        {
            get { return _popUpMessage; }
            set
            {
                _popUpMessage = value;
                OnPropertyChanged("PopUpMessage");
            }
        }

        public string SelectedSiteName {  get { return _selectedSiteName; }
            set
            {
                _selectedSiteName = value;
                OnPropertyChanged("SelectedSiteName");
            }
        }
        public string Selectedarea
        {
            get { return _selectedarea; }
            set
            {
                _selectedarea = value;
                OnPropertyChanged("Selectedarea");
            }
        }

        public bool IsBusy {
            get { return _isBusy    ; }
            set
            {
                _isBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
