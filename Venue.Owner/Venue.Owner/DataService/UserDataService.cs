﻿using Microsoft.WindowsAzure.MobileServices;
using ModernHttpClient;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Venue.Owner.Models;

namespace Venue.Owner.DataService
{
   public class UserDataService
    {
        /// <summary>
        /// Client object can be reused
        /// </summary>
        private MobileServiceClient client;

        /// <summary>
        /// Constructor initialize the client
        /// </summary>
        public UserDataService()
        {
            //Initialize the mobile service client
            client = new MobileServiceClient(StaticDataservice.BaseUrl, new NativeMessageHandler());
        }


        public async Task<UserBaseModel> GetUserByEmailOrMobile(string MobileOrEmail)
        {
            try
            {
                //Return type 
                bool isSuccess = false;
                string Error = "";
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("MobileOrEmail", MobileOrEmail.ToString());

                var registerUserResult = await client.InvokeApiAsync<UserBaseModel>("account/GetUserByEmailOrMobile", HttpMethod.Get, param);


                return registerUserResult;

            }
            catch (Exception ex)
            {
                return null;
            }
        }



     
    }
}
