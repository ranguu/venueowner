﻿using Microsoft.WindowsAzure.MobileServices;
using ModernHttpClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Venue.Owner.Models;

namespace Venue.Owner.DataService
{
    public class VenueDataService
    {
        /// <summary>
        /// Client object can be reused
        /// </summary>
        private MobileServiceClient client;

        /// <summary>
        /// Constructor initialize the client
        /// </summary>
        public VenueDataService()
        {
            //Initialize the mobile service client
            client = new MobileServiceClient(StaticDataservice.BaseUrl, new NativeMessageHandler());
          
        }


        /// <summary>
        /// To add the venue
        /// </summary>
        /// <param name="venueModel"></param>
        /// <returns></returns>
        public async Task<Tuple<bool, string>> AddVenueService(VenueModel venueModel)
        {
            try
            {
                //Return type
                bool isSuccess = false;
                string Error = "";


                var registerUserResult = await client.InvokeApiAsync<VenueModel, string>("venue/AddVenue", venueModel);

                if (registerUserResult == "Created")
                {
                    isSuccess = true;
                    Error = string.Empty;
                }
                return new Tuple<bool, string>(isSuccess, Error);

            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

        /// <summary>
        /// To add the venue
        /// </summary>
        /// <param name="venueModel"></param>
        /// <returns></returns>
        public async Task<Tuple<bool, string>> AddVenueItemService(VenueItemModel venueItemModel)
        {
            try
            {
                //Return type
                bool isSuccess = false;
                string Error = "";


                var registerUserResult = await client.InvokeApiAsync<VenueItemModel, string>("venue/AddVenueItem", venueItemModel);

                if (registerUserResult == "Created")
                {
                    isSuccess = true;
                    Error = string.Empty;
                }
                return new Tuple<bool, string>(isSuccess, Error);

            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.Message);
            }
        }



        public async Task<List<VenueModel>> GetAllVenues(Guid UserID, bool Refresh)
        {
            try
            {
                //Return type
                bool isSuccess = false;
                string Error = "";
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("UserId", UserID.ToString());

                //Check if refresh required 
                if (!Refresh && StaticDataservice.ListVenues != null )
                {
                    return StaticDataservice.ListVenues;                    
                }
                else
                {
                    StaticDataservice.ListVenues = await client.InvokeApiAsync<List<VenueModel>>("venue/GetAllVenues", HttpMethod.Get, param);
                }

                return StaticDataservice.ListVenues;

            }
            catch (Exception ex)
            {
                return new List<VenueModel>();
            }
        }
        /// <summary>
        /// To register a user
        /// </summary>
        /// <param name="user">user model</param>
        /// <returns></returns>
        public async Task<Tuple<bool,string>> RegisterUser(RegisterUserModel user)
        {
            try
            {
                //Return type
                bool isSuccess = false;
                string Error = "";


                var registerUserResult = await client.InvokeApiAsync<RegisterUserModel, string>("account/Register", user);

                if (registerUserResult == "Created")
                {
                    isSuccess = true;
                    Error = string.Empty;
                }
                return new Tuple<bool, string>(isSuccess, Error);

            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

        /// <summary>
        /// Method to user sign in
        /// </summary>
        /// <param name="username">email of the user</param>
        /// <param name="password">password of user</param>
        /// <returns></returns>
        public async Task<Tuple<bool, string>> SignInUser(SignInModel signInModel)
        {
            try
            {
                //Return type
                bool isSuccess = false;
                string Error = "";


                var registerUserResult = await client.InvokeApiAsync<SignInModel, string>("account/SignIn", signInModel);

                if (registerUserResult == "OK")
                {
                    isSuccess = true;
                    Error = string.Empty;
                }

                

                return new Tuple<bool, string>(isSuccess, Error);

            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

    }
}
