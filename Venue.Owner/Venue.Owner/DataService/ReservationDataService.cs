﻿using Microsoft.WindowsAzure.MobileServices;
using ModernHttpClient;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Venue.Owner.Models;

namespace Venue.Owner.DataService
{
    public class ReservationDataService
    {
        /// <summary>
        /// Client object can be reused
        /// </summary>
        private MobileServiceClient client;

        /// <summary>
        /// Constructor initialize the client
        /// </summary>
        public ReservationDataService()
        {
            //Initialize the mobile service client
            client = new MobileServiceClient(StaticDataservice.BaseUrl, new NativeMessageHandler());
        }


        public async Task<List<ReservationsModel>> GetAllReservation(string UserEmail)
        {
            try
            {
                //Return type 
                bool isSuccess = false;
                string Error = "";
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("userEmailAddress", UserEmail.ToString());

                var registerUserResult = await client.InvokeApiAsync<List<ReservationsModel>>("reservation/GetReservations", HttpMethod.Get, param);

                return registerUserResult;

            }
            catch (Exception ex)
            {
                return new List<ReservationsModel>();
            }
        }



        /// <summary>
        /// To add the reservation
        /// </summary>
        /// <param name="reservationModel item"></param>
        /// <returns></returns>
        public async Task<Tuple<bool, string>> AddReservationService(AddReservationModel reservationModel)
        {
            try
            {
                //Return type
                bool isSuccess = false;
                string Error = "";


                var registerUserResult = await client.InvokeApiAsync<AddReservationModel, Guid>("reservation/AddReservation", reservationModel);

                if (registerUserResult != null)
                {
                    isSuccess = true;
                    Error = string.Empty;
                }
                return new Tuple<bool, string>(isSuccess, Error);

            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.Message);
            }
        }


        /// <summary>
        /// To add the list reservations
        /// </summary>
        /// <param name="list of reservationModel items"></param>
        /// <returns></returns>
        public async Task<Tuple<bool, string>> AddReservationService(List<AddReservationModel> reservationModel)
        {
            try
            {
                //Return type
                bool isSuccess = false;
                string Error = "";

                var registerUserResult = await client.InvokeApiAsync<List<AddReservationModel>, List<Guid>>("reservation/AddReservations", reservationModel);
                if (registerUserResult != null)
                {
                    isSuccess = true;
                    Error = string.Empty;
                }
                return new Tuple<bool, string>(isSuccess, Error);

            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.Message);
            }
        }


    }
}
