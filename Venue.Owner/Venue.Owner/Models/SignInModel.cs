﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Venue.Owner.Models
{
    public class SignInModel
    {
        
        public string Email { get; set; }

        public string Password { get; set; }

        public bool RememberUser { get; set; }
    }
}
