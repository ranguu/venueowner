﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Venue.Owner.Models
{
  public  class UserBaseModel
    {

        public Guid Id { get; set; }
        public string Username { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
        public string Mobile { get; set; }


    }
}
