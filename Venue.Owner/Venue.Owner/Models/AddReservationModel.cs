﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Venue.Owner.Models
{
    public class AddReservationModel
    {
        public Guid Id { get; set; }
        public DateTime ReservationDate { get; set; }

        public int ReservationFromTime { get; set; }
        public int ReservationToTime { get; set; }
        public string QRCode { get; set; }
        //public string BookingType { get; set; }
        //public List<string> FieldTypes { get; set; }
        //public List<string> AdditionalItems { get; set; }
        public decimal Price { get; set; }
      

        public Guid? UserId { get; set; }

        public Guid? VenueItemId { get; set; }


    }
}
