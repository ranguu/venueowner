﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Venue.Owner.Models
{
    public class BookingModel : INotifyPropertyChanged
    {
        private int _id;
        private string _bookingTime;
        private bool _isReserved;
        private string _reservedByName;
        private string _reserveByNumber;
        private bool _isPaid;
        private int _fromtime;
        private int _toTime;
        private DateTime _bookingDate;
        private bool _bookNow;

        public event PropertyChangedEventHandler PropertyChanged;

        public string BookingTime { get => _bookingTime; set => _bookingTime = value; }
        public bool IsReserved { get => _isReserved; set => _isReserved = value; }
        public string ReservedByName { get => _reservedByName; set => _reservedByName = value; }
        public string ReservedByNumber { get => _reserveByNumber; set => _reserveByNumber = value; }
        public bool IsPaid { get => _isPaid; set => _isPaid = value; }

        private string _timeColor;
        public bool BookNow {

            get { if (_isReserved)
                    return false;
                else
                    return _bookNow;
            }
            set
            {
                _bookNow = value;
                OnPropertyChanged("BookNow");
                OnPropertyChanged("TimeColor");
            }

        }
        public Command<BookModel> BookSelectCommand { get; }
        public int Id { get => _id; set => _id = value; }
        public string TimeColor
        {
            get
            {
                if (_isReserved)
                {
                    return "#cae7e8";
                }
                else
                {
                    if (BookNow)
                    {
                        return "#348285";
                    }
                    else
                    {
                        return "#1e9a9e";
                    }
                }

            }

        }

        public string BookButtonText
        {
            get
            {
                if (_isReserved)
                {
                    return "view";
                }
                else
                {
                  return  "book";
                }

            }

        }

        public int Fromtime { get => _fromtime; set => _fromtime = value; }
        public int ToTime { get => _toTime; set => _toTime = value; }
        public DateTime BookingDate { get => _bookingDate; set => _bookingDate = value; }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
