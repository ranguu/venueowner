﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Venue.Owner.Models
{
    public class VenueModel
    {
        /// <summary>
        /// venue id
        /// </summary>
        public Guid VenueId { get; set; }
        /// <summary>
        /// venue name
        /// </summary>
        public string   VenueName{ get; set; }
        /// <summary>
        /// All the venue items
        /// </summary>
        public List<VenueItemModel> VenueItems{ get; set; }


        /// <summary>
        /// All the Facilities
        /// </summary>
        public List<string> Facilities { get; set; }
        /// <summary>
        /// Field open time
        /// </summary>
        public int OpenTime { get; set; }
        /// <summary>
        /// Field close time
        /// </summary>
        public int CloseTime { get; set; }
        /// <summary>
        /// Description if any
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Area of the field
        /// </summary>
        public string Area { get; set; }
        /// <summary>
        /// Sate of the field
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// Located country of field
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// Longitude of the field
        /// </summary>
        public double? Longitude { get; set; }
        /// <summary>
        /// Latitude of the field
        /// </summary>
        public double? Latitude { get; set; }

        public Guid UserId { get; set; }

        public string Address { get

            {
                if (!string.IsNullOrWhiteSpace(Country) && !string.IsNullOrWhiteSpace(State) && !string.IsNullOrWhiteSpace(Area))
                    return Country + ", " + State + ", " + Area;
                else
                    return "";
            }
        }

        public string OpenCloseTime { get
            {
                if (OpenTime != null && CloseTime != null)
                {

                    string openstr = DateTime.Today.AddMinutes(OpenTime).ToString("hh:mm tt");
                    string closestr = DateTime.Today.AddMinutes(CloseTime).ToString("hh:mm tt");

                    return openstr + " | " + closestr;
                }
                return "";

            } }
    }
}
