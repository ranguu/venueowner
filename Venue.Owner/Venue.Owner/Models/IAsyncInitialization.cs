﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Venue.Owner.Models
{
    public interface IAsyncInitialization
    {
        /// <summary>
        /// The result of the asynchronous initialization of this instance.
        /// </summary>
        Task Initialization { get; }
    }
}
