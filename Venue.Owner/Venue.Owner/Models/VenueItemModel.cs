﻿using System;
using System.Collections.Generic;

namespace Venue.Owner.Models
{
    public class VenueItemModel
    {
        public Guid Id { get; set; }
        //public string VenueItemName { get; set; }

        public string VenueSize { get; set; }
        public string Description { get; set; }

        public decimal Price { get; set; }

        public Guid VenueId { get; set; }
        public string PriceInOMR
        {
            get
            {

                return Price.ToString() + " OMR";

            }
        }
    }
}