﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Venue.Owner.Models
{
    public class RegisterUserModel
    {
        public string Name
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string PhoneNumber
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }
    }
}
