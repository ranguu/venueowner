﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Venue.Owner.Models
{
    public class VenueFacilitiesModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
