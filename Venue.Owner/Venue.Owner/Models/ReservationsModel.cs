﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Venue.Owner.Models
{
    public class ReservationsModel
    {
        public string FieldName { get; set; }
        public string Area { get; set; }
        public string BookingReference { get; set; }
        public int ReservationFrom { get; set; }
        public int ReservationTo { get; set; }
        public string QRCode { get; set; }
        public string Rating { get; set; }
        public DateTime BookedOnDate { get; set; }
        public string BookedFieldItem { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserId { get; set; }
        public Guid VenueItemId { get; set; }

        public string ReservedByName { get; set; }

        public string ReservedByNumber { get; set; }

    }
}
