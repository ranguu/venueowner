﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Venue.Owner.Models
{
    public class AuthenticationToken
    {
        public string Guid { get; set; }
        public string Access_Token { get; set; }
        public string Token_Type { get; set; }

        [JsonProperty(".expires")]
        public DateTime Expires { get; set; }
        [JsonProperty(".expires_in")]
        public DateTime ExpiresIn { get; set; }
    }
}
