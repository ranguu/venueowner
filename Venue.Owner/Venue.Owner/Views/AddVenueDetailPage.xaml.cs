﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Venue.Owner.Models;
using Venue.Owner.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Venue.Owner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddVenueDetailPage : ContentPage
    {
        private VenueModel _venueModel;
        AddVenueItemViewModel addVenueItemViewModel;
        public VenueModel VenueModel { get => _venueModel; set => _venueModel = value; }

    

        public AddVenueDetailPage(VenueModel venueModel,AllVenuesViewModel allVenuesViewModel)
        {
            _venueModel = venueModel;
            InitializeComponent();

            addVenueItemViewModel = new AddVenueItemViewModel(venueModel, allVenuesViewModel, Navigation);
            BindingContext = addVenueItemViewModel;

        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void SfCancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private void SfSize_Clicked(object sender, EventArgs e)
        {
            sfpVenueSize.IsOpen = true;
        }

        private void SfpVenueSize_OkButtonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var value = (string)e.NewValue;
                sfSize.Text = value;
                addVenueItemViewModel.VenueItem.VenueSize = value;
            }
            else
            {
                sfSize.Text = "venue size";
            }
        }
    }
}