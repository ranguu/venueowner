﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Venue.Owner.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Venue.Owner.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VenueDetailsPage : ContentPage
	{
        private double _pageHeight;
       

        public VenueDetailsPage ()
		{
            BackgroundColor = Color.FromHex("#FFFFFF");
            InitializeComponent ();
           
            
        }
        public VenueDetailsPage(VenueModel venueModel)
        {
           
            BackgroundColor = Color.FromHex("#FFFFFF");
            InitializeComponent();


        }


        //protected override async void OnAppearing()
        //{
        //    await cakeDetail.TranslateTo(0, header.Y, 500, Easing.SinOut);
        //    await header.FadeTo(1);
        //    base.OnAppearing();
        //}

        //protected override void OnSizeAllocated(double width, double height)
        //{
        //    _pageHeight = height;
        //    cakeDetail.TranslationY = _pageHeight * .65;
        //    header.FadeTo(0, 0);
        //    base.OnSizeAllocated(width, height);
        //}
        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}