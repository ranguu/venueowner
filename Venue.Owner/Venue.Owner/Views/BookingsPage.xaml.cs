﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Venue.Owner.Models;
using Venue.Owner.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Venue.Owner.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BookingsPage : ContentPage
	{
        BookingViewModel viewModel;
        public BookingsPage ()
		{
			InitializeComponent ();
            viewModel = new BookingViewModel();
            BindingContext = viewModel;

        }

        private async void SfSeeBookings_Clicked(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {

                popupLayout.IsOpen = true;
               // popUpLabel.Text = ex.Message;
            }
            
        }

        private async void SfSelectSite_Clicked(object sender, EventArgs e)
        {
           await viewModel.InitializeAsync();
             sfpSites.IsOpen = true;
        }

        private void SfSelectVenuItem_Clicked(object sender, EventArgs e)
        {
            sfpVenueItem.IsOpen = true;
        }

        private void SfpSites_SelectionChanged(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                sfSelectVenue.Text = (string)e.NewValue;
            }
            else
            {
                sfSelectVenue.Text = "select the site";
            }
        }

        private void SfpSites_OkButtonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var venue= (VenueModel)e.NewValue;
                sfSelectVenue.Text = venue.VenueName;
                viewModel.AllVenueItems = venue.VenueItems;
                viewModel.SelectedVenue = venue;
                viewModel.SelectedVenueItem = null;
                sfSelectVenuItem.Text = "select venue item";
            }
            else
            {
                sfSelectVenue.Text = "select site";
            }
        }

        private void SfpVenueItem_OkButtonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var venue = (VenueItemModel)e.NewValue;
                sfSelectVenuItem.Text = venue.Description;
                viewModel.SelectedVenueItem = venue;
            }
            else
            {
                sfSelectVenuItem.Text = "select venue item";
            }
        }

        private void SfSelectDate_Clicked(object sender, EventArgs e)
        {
            sfDatepickertest.IsOpen = true;
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {

        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            var b = (Button)sender;
            var ob = b.CommandParameter as BookingModel;
            if (ob != null)
            {
                // retrieve the value from the ‘ob’ and continue your work.
                var viewModel = (BookingViewModel)this.BindingContext;
                    viewModel.BookSelectCommand.Execute(ob);
            }
        }

        private void CheckBox_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            // retrieve the value from the ‘ob’ and continue your work.
                var viewModel = (BookingViewModel)this.BindingContext;
                viewModel.BookUnSelectCommand.Execute(sender);
            
        }

        private void ClickToShowPopup_Clicked(object sender, EventArgs e)
        {

        }

        private void SfDatepickertest_OkButtonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var venue = (ObservableCollection<object>) e.NewValue;
                sfSelectDate.Text = venue[0].ToString().ToUpper() + " - " + venue[1].ToString() + " - " + venue[2].ToString();
                var date = Convert.ToDateTime(sfSelectDate.Text);
                viewModel.SelectedDate = date;
            }
            else
            {
                sfSelectVenuItem.Text = "select venue item";
            }
        }
    }

    public class SiteInfo

    {

        private ObservableCollection<string> _sites;

        private ObservableCollection<string> _grounds;

        public ObservableCollection<string> Sites
        {
            get { return _sites; }
            set { _sites = value; }
        }

        public ObservableCollection<string> Ground
        {
            get { return _grounds; }
            set { _grounds = value; }
        }

        public SiteInfo()

        {

            Sites = new ObservableCollection<string>();
            Ground = new ObservableCollection<string>();

            Sites.Add("Al-Akuwair");
            Sites.Add("Ghala");
            Sites.Add("Salalah");
            Sites.Add("Ghobra");


            Ground.Add("11X11");
            Ground.Add("7X7");
           


        }

    }

}