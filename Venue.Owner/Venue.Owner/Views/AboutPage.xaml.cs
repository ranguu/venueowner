﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Venue.Owner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }
    }
}