﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Venue.Owner.Models;
using Venue.Owner.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Venue.Owner.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VenuePage : ContentPage
	{

        private VenueViewModel _venueviewmodel;
        private VenueModel _venueModel;
        public VenuePage (VenueModel venue, AllVenuesViewModel allVenueviewModel)
		{

            AllVenueviewModel = allVenueviewModel;
            BackgroundColor = Color.FromHex("#FFFFFF");
            _venueModel = venue;
            InitializeComponent();
            _venueviewmodel = new VenueViewModel(venue);
            BindingContext = _venueviewmodel;
        }


        
        public async void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                ((ListView)sender).SelectedItem = null;
                var selectedCover = (e.Item as BookModel).Cover;
                
                await Navigation.PushModalAsync(new VenueDetailsPage(_venueModel), true);
            }
            catch (Exception ex)
            {

                //throw;
            }

        }
        public AllVenuesViewModel AllVenueviewModel{ get; set; }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void SfAddVenueItem_Clicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushModalAsync(new AddVenueDetailPage(_venueModel, AllVenueviewModel), true);
            }
            catch (Exception ex)
            {

                //throw;
            }
        }
    }
}