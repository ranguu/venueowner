﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Venue.Owner.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Venue.Owner.Converters;

namespace Venue.Owner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddVenuePage : ContentPage
    {

        AddVenueViewModel viewModel;

        public AddVenuePage(AllVenuesViewModel allVenuesViewModel)
        {
            BackgroundColor = Color.FromHex("#FFFFFF");
            InitializeComponent();

            viewModel = new AddVenueViewModel(Navigation, allVenuesViewModel);
            BindingContext = viewModel;

        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private void SfpCountries_OkButtonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var value = (string)e.NewValue;
                sfCountry.Text = value;
                viewModel.SelectedCountry = value;
            }
            else
            {
                sfCountry.Text = "country";
            }

        }

        private void SfpStates_OkButtonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var value = (string)e.NewValue;
                sfState.Text = value;
                viewModel.SelectedState = value;
            }
            else
            {
                sfState.Text = "state";
            }
        }

        private void SfpStates_OkButtSonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var value = (string)e.NewValue;
                sfState.Text = value;
                viewModel.SelectedState = value;
            }
            else
            {
                sfState.Text = "state";
            }
        }

        private void SfpCloseTime_OkButtonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var objvalue = e.NewValue.ToDictionary();

                if (objvalue != null && objvalue.Count > 0)
                {
                    string selectedTimeText = objvalue.Where(a => a.Key == "Value").FirstOrDefault().Value.ToString();

                    TimeSpan selectedTime = (TimeSpan)objvalue.Where(a => a.Key == "Key").FirstOrDefault().Value;
                    var DicValue = new Dictionary<TimeSpan, string>();
                    DicValue.Add(selectedTime, selectedTimeText);

                    sfClose.Text = selectedTimeText;
                    viewModel.SelectedClosetime = DicValue;
                }
            }
            else
            {
                sfClose.Text = "close";
            }
        }

        private void SfpOpenTime_OkButtonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var objvalue = e.NewValue.ToDictionary();

                if (objvalue != null && objvalue.Count > 0)
                {
                    string selectedTimeText = objvalue.Where(a => a.Key == "Value").FirstOrDefault().Value.ToString();

                    TimeSpan selectedTime = (TimeSpan) objvalue.Where(a => a.Key == "Key").FirstOrDefault().Value;
                    var DicValue = new Dictionary<TimeSpan, string>();
                    DicValue.Add(selectedTime, selectedTimeText);

                     sfOpen.Text = selectedTimeText;
                    viewModel.SelectedOpentime = DicValue;
                }
            }
            else
            {
                sfOpen.Text = "open";
            }
        }

        private void SfCountry_Clicked(object sender, EventArgs e)
        {
            sfpCountries.IsOpen = true;
        }

        private void SfState_Clicked(object sender, EventArgs e)
        {
            sfpStates.IsOpen = true;
        }

        private void SfOpen_Clicked(object sender, EventArgs e)
        {
            sfpOpenTime.IsOpen = true;
        }

        private void SfClose_Clicked(object sender, EventArgs e)
        {
            sfpCloseTime.IsOpen = true;
        }

        private async void SfCancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}