﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Venue.Owner.Models;
using Venue.Owner.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Venue.Owner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllVenuesPage : ContentPage
    {
        AllVenuesViewModel allVenueviewModel;
        public AllVenuesPage()
        {
            InitializeComponent();

            allVenueviewModel = new AllVenuesViewModel();
            BindingContext = allVenueviewModel;
        }

        private void SfSelectSite_Clicked(object sender, EventArgs e)
        {
           // sfpSites.IsOpen = true;
        }

        private async void LvAllSites_ItemTapped(object sender, ItemTappedEventArgs e)
        {

            try
            {
                //((ListView)sender).SelectedItem = null;
                var selelectedVenue = (e.Item as VenueModel);
                await Navigation.PushModalAsync(new VenuePage(selelectedVenue,  allVenueviewModel), true);
            }
            catch (Exception ex)
            {

                //throw;
            }

        }

        private async void SfAddvenue_Clicked(object sender, EventArgs e)
        {
            try
            {
               await Navigation.PushModalAsync(new AddVenuePage(allVenueviewModel), true);
            }
            catch (Exception ex)
            {

                //throw;
            }
        }

        //private async void SfReloadVenue_Clicked(object sender, EventArgs e)
        //{
        //    await allVenueviewModel.InitializeAsync(); 
        //}
    }
}