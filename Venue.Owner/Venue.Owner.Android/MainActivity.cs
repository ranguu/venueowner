﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Plugin.CurrentActivity;
using ImageCircle.Forms.Plugin.Droid;
using CarouselView.FormsPlugin.Android;
using FFImageLoading.Forms.Platform;
using Syncfusion.XForms.Android.Border;

namespace Venue.Owner.Droid
{
    [Activity(Label = "Venue Owner", Icon = "@mipmap/icon",  Theme = "@style/MainTheme.Base", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            try
            {


                TabLayoutResource = Resource.Layout.Tabbar;
                ToolbarResource = Resource.Layout.Toolbar;
                Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTM4MzQzQDMxMzcyZTMyMmUzMGYvR25FOFZDWm0zN1RSOGo3VDJId3FMenh2R2RXSWlpYXd0emRXUk01Vm89");
                base.OnCreate(savedInstanceState);

                CrossCurrentActivity.Current.Init(this, savedInstanceState);
                Xamarin.Essentials.Platform.Init(this, savedInstanceState);
                global::Xamarin.Forms.Forms.SetFlags("CollectionView_Experimental");
                global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
                Syncfusion.XForms.Android.PopupLayout.SfPopupLayoutRenderer.Init();
                CarouselViewRenderer.Init();
                ImageCircleRenderer.Init();
                CachedImageRenderer.Init(true);
              
                
                LoadApplication(new App());
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}