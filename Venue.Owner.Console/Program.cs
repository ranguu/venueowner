﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Venue.Owner.DataService;
using Venue.Owner.Models;

namespace Venue.Owner.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ReservationDataService service = new ReservationDataService();
                List<AddReservationModel> addReservationModels = new List<AddReservationModel>();

                addReservationModels.Add(new AddReservationModel
                {
                    Id = Guid.NewGuid(),
                    Price = 10,
                    ReservationDate = DateTime.Now.Date,
                    ReservationFromTime = 480,
                    ReservationToTime = 540,
                    UserId = Guid.Parse("63DF381A-F4E4-E911-8417-00155D01642E"),
                    VenueItemId = Guid.Parse("f364ef42-9ce5-e911-8417-00155d01642e"),
                    QRCode = "AD234fxSW"

                });

                addReservationModels.Add(new AddReservationModel
                {
                    Id = Guid.NewGuid(),
                    Price = 10,
                    ReservationDate = DateTime.Now.Date,
                    ReservationFromTime = 480,
                    ReservationToTime = 540,
                    UserId = Guid.Parse("63DF381A-F4E4-E911-8417-00155D01642E"),
                    VenueItemId = Guid.Parse("f364ef42-9ce5-e911-8417-00155d01642e"),
                    QRCode = "AD234fxSW"

                });

                addReservationModels.Add(new AddReservationModel
                {
                    Id = Guid.NewGuid(),
                    Price = 10,
                    ReservationDate = DateTime.Now.Date,
                    ReservationFromTime = 480,
                    ReservationToTime = 540,
                    UserId = Guid.Parse("63DF381A-F4E4-E911-8417-00155D01642E"),
                    VenueItemId = Guid.Parse("f364ef42-9ce5-e911-8417-00155d01642e"),
                    QRCode = "AD234fxSW"

                });
                //  UserDataService service = new UserDataService();
                // VenueDataService service = new VenueDataService();
                //var response = service.RegisterUser(new Models.RegisterUserModel
                //{
                //    Email = "a4anu10@gmail.com",
                //    Name = "Anurag",
                //    Password = "1+2@Three",
                //    PhoneNumber = "99038552"
                //}).Result;

                //var response = service.AddVenueService(new Models.VenueModel
                //{
                //    CloseTime = new TimeSpan(23, 0, 0),
                //    Country = "Oman",
                //    Latitude = 12312,
                //    Longitude = 123123,
                //    OpenTime = new TimeSpan(8, 0, 0),
                //    VenueName = "NewName",
                //    UserId = Guid.Parse("63DF381A-F4E4-E911-8417-00155D01642E"),
                //    Area="Ghala",
                //    State="Muscat",
                //    Description="Description"


                //}).Result;

                //var response = service.AddVenueItemService(new Models.VenueItemModel
                //{
                //    Description = "Description",
                //    Id= Guid.NewGuid(),
                //    Price=2,
                //    VenueId= Guid.Parse("471A5E84-F4E4-E911-8417-00155D01642E"),
                //    VenueSize="_5x5"
                //}).Result;

                //var response = service.GetAllVenues(Guid.Parse("63DF381A-F4E4-E911-8417-00155D01642E"),true).Result;
                //var response = service.GetAllReservation("a4anu10@gmail.com").Result;
                // var response = service.GetUserByEmailOrMobile("a4anu10@gmail.com").Result;



                var response = service.AddReservationService(addReservationModels).Result;

            }
            catch (Exception ex)
            {

                throw ex;
            }

           
        }
    }
}
